from utils import initial_config, importing_data


def main():
    initial_config.configure_logging()
    args = initial_config.define_args()
    importing_data.check_file_extension(args.file)
    importing_data.clean_whatsapp_logs(args.file)
    importing_data.parse_whatsapp_logs()


if __name__ == "__main__":
    main()
