import os.path
import pandas as pd
import re
import logging


def check_file_extension(path):
    """
    Checks whether file extension is .txt or .html
    """
    _, extension = os.path.splitext(path)
    if extension == ".txt" or extension == ".csv":
        logging.debug(f"Given file extension is correct {extension}")
        return path
    else:
        logging.critical(
            f"Wrong file extension ({extension}), correct extension is '.txt'"
        )
        raise ValueError(f"Wrong file extension, .txt is correct ({extension} given)")


def clean_whatsapp_logs(input_path):
    """
    Function reads a WhatsApp log file, cleans it, and writes the cleaned data to a new file named 'cleaned.txt'.
    """
    with open(input_path) as file:
        correct_lines_counter = 0
        # Regex for DD.MM.YYYY, HH:MM format
        regex_pattern = (
            r"^[0-3][0-9]\.[0-1][0-9]\.[1-2][0-9][0-9][0-9],\s[0-2][0-9]:[0-5][0-9]"
        )
        regex = re.compile(regex_pattern)

        with open("cleaned.txt", "w") as cleaned_file:
            for line in file.readlines():
                if regex.match(line) is None:
                    cleaned_file.write(line.strip("\n"))
                else:
                    cleaned_file.write("\n" + line.rstrip("\n"))
                    correct_lines_counter += 1
        logging.debug("Succesfully cleaned a file")
        if correct_lines_counter == 0:
            logging.critical(
                "The supplied file is not a WhatsApp logbook. Correct lines count = 0"
            )
            raise ValueError(
                "This is not a WhatsApp log file. Please supply a correct file."
            )


def parse_whatsapp_logs():
    """
    Function reads the cleaned WhatsApp log file and parses it
    """
    data = pd.DataFrame(open("cleaned.txt"), columns=["Date"])
    logging.info("Successfully imported data to DataFrame")
    data[["Date", "Name"]] = data["Date"].str.split("-", n=1, expand=True)
    logging.info("Successfully splited column `Date` to `Date` and `Name`")
    data[["Name", "Message"]] = data["Name"].str.split(":", n=1, expand=True)
    logging.info("Successfully splited column `Name` to `Name` and `Message`")
    data["Message"] = data["Message"].str.strip("\n")
    logging.info("Stripped messages of trailing \\n")
    print(data.dropna(axis=0))
    data = data.dropna(axis=0)
    return data
