# This file contains functions that configure behaviourof libraries before the main part of the project starts
import argparse
import logging
import os.path


def check_file(path):
    """
    Checks whether path to the file is correct
    """
    if os.path.isfile(path):
        logging.debug(f"Given file path is correct (given path: {path})")
        return path
    else:
        logging.critical(f"Given file path is incorrect (given path: {path})")
        raise argparse.ArgumentError(
            message=f"{path} is not a valid file path", argument=None
        )


def define_args():
    """
    Defines command line arguments to be inputted by user while launching application
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("file", help="Path to the file", type=check_file)
    args = parser.parse_args()
    logging.debug("Parsed args")
    return args


def configure_logging():
    try:
        logging.basicConfig(
            level=logging.DEBUG,
            filename="console.log",
            format="%(levelname)s : %(asctime)s : %(message)s",
        )
        logging.debug("Succesfully formatted logging")
    except:
        logging.warning("Could not configure logging library")
